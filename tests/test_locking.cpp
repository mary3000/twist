#include <twist/support/locking.hpp>

#include <twist/thread/spinlock.hpp>
#include <twist/test_framework/test_framework.hpp>

#include <mutex>
#include <shared_mutex>
#include <thread>

class SpinLock {
 public:
  void Lock() {
    while (locked_.exchange(true)) {
      twist::thread::SpinLockPause();
    }
  }

  bool IsLocked() const {
    return locked_.load();
  }

  void Unlock() {
    locked_.exchange(false);
  }

  // BasicLockable

  void lock() {
    Lock();
  }

  void unlock() {
    Unlock();
  }

 private:
  std::atomic<bool> locked_{false};
};

TEST_SUITE(Locking) {
  SIMPLE_TEST(Exclusive) {
    SpinLock spinlock;
    ASSERT_FALSE(spinlock.IsLocked());
    auto lock = twist::LockUnique(spinlock);
    ASSERT_TRUE(spinlock.IsLocked());
    lock.unlock();
    ASSERT_FALSE(spinlock.IsLocked());
  }

  SIMPLE_TEST(Shared) {
    std::shared_timed_mutex rwmutex;

    auto rlock = twist::LockShared(rwmutex);

    auto routine = [&]() {
        ASSERT_FALSE(rwmutex.try_lock());
        auto that_rwlock = twist::LockShared(rwmutex);
    };

    std::thread t(routine);
    t.join();
  }
}
