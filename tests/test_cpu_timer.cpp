#include <twist/test_utils/cpu_timer.hpp>

#include <twist/test_framework/test_framework.hpp>

#include <chrono>
#include <thread>

using namespace std::chrono_literals;

TEST_SUITE(CPUTimer) {
  SIMPLE_TEST(Work) {
    CPUTimer timer;
    volatile size_t count = 0;
    for (size_t i = 0; i < 100000000; ++i) {
      ++count;
    }
    ASSERT_TRUE(timer.RunningTime() > 0.1);
  }

  SIMPLE_TEST(Sleep) {
    CPUTimer timer;
    std::this_thread::sleep_for(100ms);
    ASSERT_TRUE(timer.RunningTime() < 0.01);
  }
}
