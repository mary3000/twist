#include <twist/fiber/core/fiber.hpp>

#include <twist/fiber/core/scheduler.hpp>
#include <twist/fiber/core/stack_allocator.hpp>

#include <twist/fiber/context/asan.hpp>

#include <twist/support/compiler.hpp>
#include <twist/support/sequencer.hpp>

namespace twist {
namespace fiber {

//////////////////////////////////////////////////////////////////////

static FiberId GenerateId() {
  static Sequencer sequencer;
  return sequencer.Next();
}

Fiber* Fiber::Create(FiberRoutine routine) {
  auto* fiber = new Fiber();

  fiber->stack_ = AllocateStack();
  fiber->id_ = GenerateId();
  fiber->routine_ = std::move(routine);
  fiber->state_ = FiberState::Starting;

  SetupTrampoline(fiber);

  return fiber;
}

Fiber::~Fiber() {
  ReleaseStack(std::move(stack_));
}

//////////////////////////////////////////////////////////////////////

static void FiberTrampoline() {
  AsanAfterSwitch();

  Fiber* self = GetCurrentFiber();

  self->SetState(FiberState::Running);

  try {
    self->InvokeUserRoutine();
  } catch (...) {
    TWIST_PANIC("Uncaught exception in fiber " << self->Id());
  }

  GetCurrentScheduler()->Terminate();  // never returns

  TWIST_UNREACHABLE();
}

void Fiber::SetupTrampoline(Fiber* fiber) {
  fiber->context_.Setup(
      /*stack=*/fiber->stack_.AsMemSpan(),
      /*trampoline=*/FiberTrampoline);
}

}  // namespace fiber
}  // namespace twist
