#pragma once

#include <twist/fiber/core/api.hpp>
#include <twist/fiber/core/fiber.hpp>
#include <twist/fiber/core/sleep_queue.hpp>

#include <twist/thread/stack.hpp>

#include <twist/support/time.hpp>

namespace twist {
namespace fiber {

using FiberQueue = IntrusiveList<Fiber>;

class Scheduler {
 public:
  Scheduler();

  // 'fuel' limits number of scheduling iterations (-1 means no limit)
  void Run(FiberRoutine main, size_t fuel = -1);

  // System calls
  // Called in fiber context

  void Spawn(FiberRoutine routine);
  void Yield();
  void SleepFor(Duration duration);
  void Suspend();
  void Resume(Fiber* that);
  void Terminate();

  // Ignore upcoming Yield calls
  void PreemptDisable(bool disable = true) {
    preempt_disabled_ = disable;
  }

  // Statistics for tests
  size_t SwitchCount() const {
    return switch_count_;
  }

 private:
  void RunLoop(size_t fuel);
  void CheckDeadlock();

  bool IsIdle() const;
  void Poller();
  void PollSleepQueue();

  // Context switch: current fiber -> scheduler
  void SwitchToScheduler();

  Fiber* PickReadyFiber();
  // Context switch: scheduler -> fiber
  void SwitchTo(Fiber* fiber);
  void Reschedule(Fiber* fiber);
  void Schedule(Fiber* fiber);

  Fiber* CreateFiber(FiberRoutine routine);
  void Destroy(Fiber* fiber);

 private:
  thread::Stack stack_;
  ExecutionContext loop_context_;
  FiberQueue run_queue_;
  SleepQueue sleep_queue_;
  size_t alive_count_{0};

  bool preempt_disabled_{false};

  // Statistics
  size_t switch_count_{0};
};

//////////////////////////////////////////////////////////////////////

Fiber* GetCurrentFiber();
Scheduler* GetCurrentScheduler();

}  // namespace fiber
}  // namespace twist
