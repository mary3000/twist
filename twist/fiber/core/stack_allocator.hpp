#pragma once

#include <twist/fiber/context/stack.hpp>

namespace twist {
namespace fiber {

Stack AllocateStack();
void ReleaseStack(Stack stack);

void SetStackSize(size_t pages);

}  // namespace fiber
}  // namespace twist
