#include <twist/fiber/core/stack_allocator.hpp>

#include <vector>

namespace twist {
namespace fiber {

class StackAllocator {
  static const size_t kStackSizePages = 8;

 public:
  StackAllocator()
    : size_pages_(kStackSizePages) {
  }

  Stack Allocate() {
    if (!pool_.empty()) {
      Stack stack = std::move(pool_.back());
      pool_.pop_back();
      return stack;
    }
    return Stack::Allocate(size_pages_);
  }

  void Release(Stack stack) {
    pool_.push_back(std::move(stack));
  }

  void SetSize(size_t pages) {
    size_pages_ = pages;
  }

 private:
  std::vector<Stack> pool_;
  size_t size_pages_;
};

//////////////////////////////////////////////////////////////////////

static StackAllocator allocator;

Stack AllocateStack() {
  return allocator.Allocate();
}

void ReleaseStack(Stack stack) {
  allocator.Release(std::move(stack));
}

void SetStackSize(size_t pages) {
  allocator.SetSize(pages);
}

}  // namespace fiber
}  // namespace twist
