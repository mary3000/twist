#include <twist/fiber/core/scheduler.hpp>

#include <twist/support/compiler.hpp>
#include <twist/support/random.hpp>

#include <thread>

namespace twist {
namespace fiber {

//////////////////////////////////////////////////////////////////////

static thread_local Fiber* current_fiber{nullptr};

Fiber* GetCurrentFiber() {
  return current_fiber;
}

static inline Fiber* GetAndResetCurrentFiber() {
  auto* f = current_fiber;
  current_fiber = nullptr;
  return f;
}

static inline void SetCurrentFiber(Fiber* f) {
  current_fiber = f;
}

//////////////////////////////////////////////////////////////////////

static thread_local Scheduler* current_scheduler;

Scheduler* GetCurrentScheduler() {
  TWIST_VERIFY(current_scheduler, "not in fiber context");
  return current_scheduler;
}

struct SchedulerScope {
  SchedulerScope(Scheduler* scheduler) {
    TWIST_VERIFY(!current_scheduler,
                 "cannot run scheduler from another scheduler");
    current_scheduler = scheduler;
  }

  ~SchedulerScope() {
    current_scheduler = nullptr;
  }
};

//////////////////////////////////////////////////////////////////////

Scheduler::Scheduler() : stack_(thread::Stack::ThisThread()) {
  loop_context_.Setup(stack_.AsMemSpan());
}

// Operations invoked by running fibers

void Scheduler::SwitchToScheduler() {
  Fiber* caller = GetAndResetCurrentFiber();
  caller->Context().SwitchTo(loop_context_);
}

// Syscalls

void Scheduler::Spawn(FiberRoutine routine) {
  auto* created = CreateFiber(routine);
  Schedule(created);
}

void Scheduler::Yield() {
  if (preempt_disabled_) {
    return;
  }
  Fiber* caller = GetCurrentFiber();
  caller->SetState(FiberState::Runnable);
  SwitchToScheduler();
}

void Scheduler::SleepFor(Duration duration) {
  Fiber* caller = GetCurrentFiber();

  bool first = sleep_queue_.IsEmpty();
  sleep_queue_.Put(caller, duration);
  if (first) {
    Spawn([this]() { Poller(); });
  }
  caller->SetState(FiberState::Suspended);
  SwitchToScheduler();
}

void Scheduler::Suspend() {
  Fiber* caller = GetCurrentFiber();
  caller->SetState(FiberState::Suspended);
  SwitchToScheduler();
}

void Scheduler::Resume(Fiber* that) {
  that->SetState(FiberState::Runnable);
  Schedule(that);
}

void Scheduler::Terminate() {
  Fiber* caller = GetCurrentFiber();
  caller->SetState(FiberState::Terminated);
  SwitchToScheduler();
}

// Scheduling

void Scheduler::Run(FiberRoutine main, size_t fuel) {
  SchedulerScope scope(this);
  Spawn(main);
  RunLoop(fuel);
  CheckDeadlock();
}

void Scheduler::RunLoop(size_t fuel) {
  while (!run_queue_.IsEmpty() && fuel-- > 0) {
    Fiber* next = PickReadyFiber();
    SwitchTo(next);
    Reschedule(next);
  }
}

void Scheduler::CheckDeadlock() {
  if (run_queue_.IsEmpty() && alive_count_ > 0) {
    throw DeadlockDetected();
  }
}

void Scheduler::PollSleepQueue() {
  auto now = SleepQueue::Clock::now();
  while (Fiber* f = sleep_queue_.Poll(now)) {
    Resume(f);
  }
}

bool Scheduler::IsIdle() const {
  return run_queue_.IsEmpty() && !sleep_queue_.IsEmpty();
}

void Scheduler::Poller() {
  while (!sleep_queue_.IsEmpty()) {
    PollSleepQueue();

    if (IsIdle()) {
      std::this_thread::sleep_until(sleep_queue_.NextDeadLine());
      continue;
    } else if (!sleep_queue_.IsEmpty()) {
      Yield();  // Reschedule poller
    } else {
      break;
    }
  }
}

Fiber* Scheduler::PickReadyFiber() {
#if defined(TWIST_FAULTY)
  return UnlinkRandomItem(run_queue_);
#else
  return run_queue_.PopFront();
#endif
}

void Scheduler::SwitchTo(Fiber* fiber) {
  ++switch_count_;
  SetCurrentFiber(fiber);
  fiber->SetState(FiberState::Running);
  // scheduler loop_context_ -> fiber->context_
  loop_context_.SwitchTo(fiber->Context());
}

void Scheduler::Reschedule(Fiber* fiber) {
  switch (fiber->State()) {
    case FiberState::Runnable:  // From Yield
      Schedule(fiber);
      break;
    case FiberState::Suspended:  // From Suspend
      // Do nothing
      break;
    case FiberState::Terminated:  // From Terminate
      Destroy(fiber);
      break;
    default:
      TWIST_PANIC("Unexpected fiber state");
      break;
  }
}

void Scheduler::Schedule(Fiber* fiber) {
  run_queue_.PushBack(fiber);
}

Fiber* Scheduler::CreateFiber(FiberRoutine routine) {
  ++alive_count_;
  return Fiber::Create(routine);
}

void Scheduler::Destroy(Fiber* fiber) {
  --alive_count_;
  delete fiber;
}

}  // namespace fiber
}  // namespace twist
