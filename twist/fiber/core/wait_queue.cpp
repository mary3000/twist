#include <twist/fiber/core/wait_queue.hpp>

#include <twist/fiber/core/scheduler.hpp>

#include <twist/support/random.hpp>

namespace twist {
namespace fiber {

static inline void Suspend() {
  GetCurrentScheduler()->Suspend();
}

static inline void Resume(Fiber* f) {
  GetCurrentScheduler()->Resume(f);
}

class WaitQueue::Impl {
 public:
  void Park() {
    Fiber* self = GetCurrentFiber();
    wait_queue_.PushBack(self);
    Suspend();
  }

  void WakeOne() {
    if (wait_queue_.IsEmpty()) {
      return;
    }
#if defined(TWIST_FAULTY)
    Fiber* f = UnlinkRandomItem(wait_queue_);
#else
    Fiber* f = wait_queue_.PopFront();
#endif
    Resume(f);
  }

  void WakeAll() {
#if defined(TWIST_FAULTY)
    auto shuffled = ShuffleToVector(wait_queue_);
    for (Fiber* f : shuffled) {
      Resume(f);
    }
#else
    while (!wait_queue_.IsEmpty()) {
      Fiber* f = wait_queue_.PopFront();
      Resume(f);
    }
#endif
  }

  bool IsEmpty() const {
    return wait_queue_.IsEmpty();
  }

 private:
  FiberQueue wait_queue_;
};

WaitQueue::WaitQueue() : pimpl_(std::make_unique<Impl>()) {
}

WaitQueue::~WaitQueue() {
}

void WaitQueue::Park() {
  pimpl_->Park();
}

void WaitQueue::WakeOne() {
  pimpl_->WakeOne();
}

void WaitQueue::WakeAll() {
  pimpl_->WakeAll();
}

bool WaitQueue::IsEmpty() const {
  return pimpl_->IsEmpty();
}

}  // namespace fiber
}  // namespace twist
