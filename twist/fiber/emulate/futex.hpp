#pragma once

#include <twist/fiber/core/wait_queue.hpp>

#include <atomic>

namespace twist {
namespace fiber {

class FutexLike {
 public:
  FutexLike(uint32_t* addr) : addr_(addr) {
  }

  FutexLike(std::atomic_uint32_t& state) : FutexLike((uint32_t*)&state) {
  }

  // returns true if waked by Wake, false otherwise
  bool Wait(uint32_t expected) {
    if (*addr_ == expected) {
      wait_queue_.Park();
      return true;
    } else {
      return false;
    }
  }

  void WakeOne() {
    wait_queue_.WakeOne();
  }

  void WakeAll() {
    wait_queue_.WakeAll();
  }

 private:
  uint32_t* addr_;
  WaitQueue wait_queue_;
};

}  // namespace fiber
}  // namespace twist
