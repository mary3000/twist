#pragma once

#include <twist/support/compiler.hpp>
#include <twist/memory/memspan.hpp>

#include <sanitizer/asan_interface.h>

#if __has_feature(address_sanitizer)
#pragma message("Annotate stack switch for address sanitizer")
#endif

namespace twist {
namespace fiber {

// Annotate fiber switch for address sanitizer

inline void AsanBeforeSwitchTo(void* bottom, size_t size) {
#if __has_feature(address_sanitizer)
  __sanitizer_start_switch_fiber(nullptr, bottom, size);
#else
  TWIST_UNUSED(bottom);
  TWIST_UNUSED(size);
#endif
}

inline void AsanBeforeSwitchTo(const MemSpan& stack) {
  AsanBeforeSwitchTo(stack.Back(), stack.Size());
}

inline void AsanAfterSwitch() {
#if __has_feature(address_sanitizer)
  __sanitizer_finish_switch_fiber(nullptr, nullptr, nullptr);
#endif
}

}  // namespace fiber
}  // namespace twist
