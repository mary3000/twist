#include <twist/fiber/context/stack.hpp>

#include <twist/support/assert.hpp>

#include <utility>

namespace twist {
namespace fiber {

Stack::Stack(MmapAllocation allocation) : allocation_(std::move(allocation)) {
}

Stack Stack::Allocate(size_t pages) {
  TWIST_VERIFY(pages > 0, "pages == 0");

  MmapAllocation allocation = MmapAllocation::AllocatePages(pages);
  allocation.ProtectPages(/*offset=*/0, /*count=*/1);

  return Stack{std::move(allocation)};
}

char* Stack::Bottom() const {
  return (char*)((std::uintptr_t*)allocation_.End() - 1);
}

MemSpan Stack::AsMemSpan() const {
  return allocation_.AsMemSpan();
}

}  // namespace fiber
}  // namespace twist
