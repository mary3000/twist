#pragma once

#include <twist/fiber/core/wait_queue.hpp>
#include <twist/fiber/sync/mutex.hpp>

// std::unique_lock
#include <mutex>

namespace twist {
namespace fiber {

class ConditionVariable {
 public:
  void Wait(Mutex& mutex) {
    mutex.Unlock();
    wait_queue_.Park();
    mutex.Lock();
  }

  void NotifyOne() {
    wait_queue_.WakeOne();
  }

  void NotifyAll() {
    wait_queue_.WakeAll();
  }

  // std::condition_variable interface

  using Lock = std::unique_lock<Mutex>;

  void wait(Lock& lock) {  // NOLINT
    Wait(*lock.mutex());
  }

  template <typename Predicate>
  void wait(Lock& lock, Predicate predicate) {  // NOLINT
    while (!predicate()) {
      wait(lock);
    }
  }

  void notify_one() {  // NOLINT
    NotifyOne();
  }

  void notify_all() {  // NOLINT
    NotifyAll();
  }

 private:
  WaitQueue wait_queue_;
};

}  // namespace fiber
}  // namespace twist
