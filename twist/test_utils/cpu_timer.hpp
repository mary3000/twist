#pragma once

#include <ctime>

class CPUTimer {
 public:
  CPUTimer() {
    Reset();
  }

  void Reset() {
    start_ts_ = std::clock();
  }

  double RunningTime() const {
    const size_t clocks = std::clock() - start_ts_;
    return ClocksToSeconds(clocks);
  }

 private:
  static double ClocksToSeconds(const size_t clocks) {
    return 1.0 * clocks / CLOCKS_PER_SEC;
  }

 private:
  std::clock_t start_ts_;
};
