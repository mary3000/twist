#pragma once

#include <twist/test_framework/test_framework.hpp>
#include <twist/strand/stdlike.hpp>
#include <twist/test_utils/affinity.hpp>

#include <functional>
#include <vector>

namespace twist {
namespace test_utils {

class ScopedExecutor {
  using Task = std::function<void()>;

 public:
  void Submit(Task task);

  template <typename... Args>
  void Submit(Args&&... args) {
    Submit(Task(std::bind(std::forward<Args>(args)...)));
  }

  ~ScopedExecutor() {
    Join();
  }

  void Join();

 private:
  static void Run(Task task);

 private:
  std::vector<twist::strand::thread> threads_;
  bool joined_{false};
};

////////////////////////////////////////////////////////////////////////////////

template <typename... Args>
twist::strand::thread RunThread(Args&&... args) {
  std::function<void()> task(std::bind(std::forward<Args>(args)...));

  auto wrapped_task = [task]() {
    SetTestThreadAffinity();
    try {
      task();
    } catch (...) {
      FailTestByException();
    }
  };

  return twist::strand::thread(wrapped_task);
}

}  // namespace test_utils
}  // namespace twist
