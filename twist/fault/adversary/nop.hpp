#pragma once

#include <twist/fault/adversary/adversary.hpp>

namespace twist {
namespace fault {

IAdversaryPtr CreateNopAdversary();

}  // namespace fault
}  // namespace twist
