#pragma once

#include <twist/fault/adversary/random_event.hpp>

#include <twist/strand/stdlike.hpp>

#include <atomic>

namespace twist {
namespace fault {

/////////////////////////////////////////////////////////////////////

class Yielder {
 public:
  Yielder(size_t freq) : yield_(freq) {
  }

  void Reset() {
    yield_count_ = 0;
  }

  void MaybeYield() {
    if (yield_.Test()) {
      twist::strand::this_thread::yield();
      yield_count_.fetch_add(1, std::memory_order_relaxed);
    }
  }

  size_t YieldCount() const {
    return yield_count_.load(std::memory_order_relaxed);
  }

 private:
  RandomEvent yield_;

  // statistics
  std::atomic<size_t> yield_count_{0};
};

}  // namespace fault
}  // namespace twist
