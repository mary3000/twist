#include "mutex.hpp"

#include <twist/fault/adversary/inject_fault.hpp>

#include <twist/support/assert.hpp>

namespace twist {
namespace fault {

FaultyMutex::FaultyMutex() : owner_id_{strand::kInvalidThreadId} {
  AccessAdversary();
}

void FaultyMutex::lock() {  // NOLINT
  InjectFault();
  impl_.lock();
  owner_id_ = strand::this_thread::get_id();
  InjectFault();
}

bool FaultyMutex::try_lock() {  // NOLINT
  InjectFault();
  bool acquired = impl_.try_lock();
  if (acquired) {
    owner_id_ = strand::this_thread::get_id();
  }
  InjectFault();
  return acquired;
}

void FaultyMutex::unlock() {  // NOLINT
  InjectFault();
  TWIST_VERIFY(owner_id_ != strand::kInvalidThreadId,
               "Cannot unlock unlocked mutex");
  TWIST_VERIFY(owner_id_ == strand::this_thread::get_id(),
               "Cannot unlock mutex from another thread");
  owner_id_ = strand::kInvalidThreadId;
  impl_.unlock();
  InjectFault();
}

}  // namespace fault
}  // namespace twist
