#pragma once

#include <twist/strand/stdlike.hpp>

#include <functional>

namespace twist {
namespace fault {

using ThreadRoutine = std::function<void()>;

class FaultyThread {
 public:
  FaultyThread(ThreadRoutine routine);

  // NOLINTNEXTLINE
  void join() {
    impl_.join();
  }

  // NOLINTNEXTLINE
  bool joinable() const {
    return impl_.joinable();
  }

  // NOLINTNEXTLINE
  void detach() {
    impl_.detach();
  }

 private:
  twist::strand::thread impl_;
};

}  // namespace fault
}  // namespace twist
