#pragma once

#include <twist/strand/futex.hpp>

#include <twist/fault/adversary/inject_fault.hpp>
#include <twist/fault/wrappers/atomic.hpp>

namespace twist {
namespace fault {

class FaultyFutex {
 public:
  FaultyFutex(uint32_t* addr) : impl_(addr) {
  }

  FaultyFutex(FaultyAtomic<uint32_t>& state)
      : impl_(reinterpret_cast<uint32_t*>(&state)) {
  }

  void Wait(uint32_t expected) {
    InjectFault();
    impl_.Wait(expected);
    InjectFault();
  }

  void WakeOne() {
    InjectFault();
    impl_.WakeOne();
    InjectFault();
  }

  void WakeAll() {
    InjectFault();
    impl_.WakeAll();
    InjectFault();
  }

 private:
  strand::Futex impl_;
};

}  // namespace fault
}  // namespace twist
