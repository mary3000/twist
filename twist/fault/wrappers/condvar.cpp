#include <twist/fault/wrappers/condvar.hpp>

#include <twist/fault/adversary/inject_fault.hpp>

#include <twist/support/intrusive_list.hpp>
#include <twist/support/one_shot_event.hpp>
#include <twist/support/random.hpp>

#include <twist/fiber/core/scheduler.hpp>

#include <list>
#include <memory>
#include <mutex>
#include <thread>
#include <vector>

namespace twist {
namespace fault {

#if defined(TWIST_FIBER)

/////////////////////////////////////////////////////////////////////

// Fiber implementation

using namespace twist::fiber;

// Disable fiber preemption in current scope
struct PreemptionGuard {
  PreemptionGuard() {
    GetCurrentScheduler()->PreemptDisable(true);
  }

  ~PreemptionGuard() {
    GetCurrentScheduler()->PreemptDisable(false);
  }
};

class FaultyConditionVariable::Impl {
 public:
  void Wait(Lock& lock) {
    if (++wait_call_count_ % 13 == 0) {
      return;
    }

    InjectFault();
    {
      PreemptionGuard guard;
      lock.unlock();
    }
    Park();
    InjectFault();
    lock.lock();
  }

  void NotifyOne() {
    InjectFault();
    if (!wait_queue_.IsEmpty()) {
      ResumeOne();
    }
    InjectFault();
  }

  void NotifyAll() {
    InjectFault();
    ResumeAll();
    InjectFault();
  }

 private:
  void Park() {
    Fiber* caller = GetCurrentFiber();
    wait_queue_.PushBack(caller);
    GetCurrentScheduler()->Suspend();
  }

  void ResumeOne() {
    Fiber* f = UnlinkRandomItem(wait_queue_);
    GetCurrentScheduler()->Resume(f);
  }

  void ResumeAll() {
    auto shuffled = ShuffleToVector(wait_queue_);
    for (Fiber* f : shuffled) {
      GetCurrentScheduler()->Resume(f);
    }
  }

 private:
  fiber::FiberQueue wait_queue_;
  size_t wait_call_count_{0};
};

#else

/////////////////////////////////////////////////////////////////////

// Thread implementation

class FaultyConditionVariable::Impl {
 private:
  struct WaitNode : public IntrusiveListNode<WaitNode>,
                    public twist::OneShotEvent {
    void Wake() {
      Set();
    }
  };

  using WaitQueue = IntrusiveList<WaitNode>;

 public:
  void Wait(Lock& lock) {
    ++wait_call_count_;

    // emulate spurious wakeups
    if (wait_call_count_ % 13 == 0) {
      return;  // instant wakeup
    }

    {
      // wait implementation

      std::unique_lock<std::mutex> queue_lock(queue_mutex_);

      // release thread's lock
      lock.unlock();

      // allocate on stack!
      WaitNode wait_node;

      queue_.PushBack(&wait_node);

      queue_lock.unlock();

      // await signal from notify_one/notify_all
      wait_node.Await();

      // acquire thread's lock back
      lock.lock();
    }

    if (wait_call_count_ % 5 == 0) {
      // give lock to contender thread, try to provoke intercepted wakeup
      lock.unlock();
      std::this_thread::yield();
      lock.lock();
    }
  }

  // notify random thread from waiting queue

  void NotifyOne() {
    std::unique_lock<std::mutex> queue_lock(queue_mutex_);

    if (queue_.IsEmpty()) {
      return;
    }

    auto* wait_node = UnlinkRandomItem(queue_);

    queue_lock.unlock();

    wait_node->Wake();
  }

  // notify all waiting threads in random order

  void NotifyAll() {
    // grab whole waiting queue
    std::unique_lock<std::mutex> queue_lock(queue_mutex_);
    auto queue = std::move(queue_);
    queue_lock.unlock();

    auto wake_order = ShuffleToVector(queue);

    for (WaitNode* wait_node : wake_order) {
      wait_node->Wake();
    }
  }

 private:
  WaitQueue queue_;
  std::mutex queue_mutex_;

  // just size_t, we don't expect concurrent wait invocations
  size_t wait_call_count_{0};
};

#endif

/////////////////////////////////////////////////////////////////////

FaultyConditionVariable::FaultyConditionVariable()
    : pimpl_(std::make_unique<FaultyConditionVariable::Impl>()) {
  AccessAdversary();
}

FaultyConditionVariable::~FaultyConditionVariable() {
}

void FaultyConditionVariable::wait(Lock& lock) {  // NOLINT
  pimpl_->Wait(lock);
}

void FaultyConditionVariable::notify_one() {  // NOLINT
  pimpl_->NotifyOne();
}

void FaultyConditionVariable::notify_all() {  // NOLINT
  pimpl_->NotifyAll();
}

}  // namespace fault
}  // namespace twist
