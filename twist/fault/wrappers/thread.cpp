#include <twist/fault/wrappers/thread.hpp>

// TODO: move to fault
#include <twist/test_utils/affinity.hpp>

namespace twist {
namespace fault {

FaultyThread::FaultyThread(ThreadRoutine routine)
    : impl_([routine]() {
        SetTestThreadAffinity();
        routine();
      }) {
}

}  // namespace fault
}  // namespace twist