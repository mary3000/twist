#include <twist/test_framework/execute_test.hpp>

#include <twist/test_framework/fail_handler.hpp>
#include <twist/test_framework/test_framework.hpp>
#include <twist/test_framework/helpers.hpp>

#include <twist/support/compiler.hpp>
#include <twist/support/sync_output.hpp>
#include <twist/logging/logging.hpp>

#if UNIX && TWIST_FORK_TESTS

////////////////////////////////////////////////////////////////////////////////

// Execute test in forked subprocess

#include <twist/test_framework/fork.hpp>

class ForkedTestFailHandler : public ITestFailHandler {
 public:
  void Fail(ITestPtr test, const std::string& error) override {
    TWIST_UNUSED(test);
    twist::FlushPendingLogMessages();
    std::cerr << error << std::endl << std::flush;
    std::abort();
  }
};

static void InstallForkedTestFailHandler() {
  InstallTestFailHandler(std::make_shared<ForkedTestFailHandler>());
}

static void ExecuteTestInForkedProcess(ITestPtr test) {
  InstallForkedTestFailHandler();

  try {
    test->Run();
  } catch (...) {
    FailTestByException();
  }

  twist::FlushPendingLogMessages();
}

class PrintStdoutConsumer : public twist::IByteStreamConsumer {
 public:
  void Consume(const char* buf, size_t length) override {
    std::cout.write(buf, length);
    bytes_consumed_ += length;
  }

  void Eof() override {
    if (bytes_consumed_ > 0) {
      std::cout << std::endl;
    }
  }

 private:
  size_t bytes_consumed_ = 0;
};

static void ExecuteTestWithFork(ITestPtr test) {
  auto execute_test = [test]() { ExecuteTestInForkedProcess(test); };

  auto result = twist::ExecuteWithFork(
      execute_test, std::make_unique<PrintStdoutConsumer>(), nullptr);

  // Process result

  const auto& stderr = result.GetStderr();

  int exit_code;
  if (result.Exited(exit_code)) {
    if (exit_code != 0) {
      FAIL_TEST("Test subprocess terminated with non-zero exit code: "
                << exit_code
                << ", stderr: " << twist::FormatStderrForErrorMessage(stderr));
    }
  }

  int signal;
  if (result.Signaled(signal)) {
    if (stderr.empty()) {
      FAIL_TEST("Test subprocess terminated by signal: " << signal);
    } else {
      FAIL_TEST("Test subprocess terminated by signal "
                << signal
                << ", stderr: " << twist::FormatStderrForErrorMessage(stderr));
    }
  }

  if (!stderr.empty()) {
    FAIL_TEST(
        "Test produced stderr: " << twist::FormatStderrForErrorMessage(stderr));
  }

  // Test completed!
}

void ExecuteTest(ITestPtr test) {
  ExecuteTestWithFork(std::move(test));
}

#else

////////////////////////////////////////////////////////////////////////////////

// Execute test locally

static void ExecuteTestInCurrentProcess(ITestPtr test) {
  test->Run();
}

void ExecuteTest(ITestPtr test) {
  ExecuteTestInCurrentProcess(std::move(test));
}

#endif
