#pragma once

#include <string_view>

std::string_view GetSuccessEmoji();
std::string_view GetFailEmoji();
