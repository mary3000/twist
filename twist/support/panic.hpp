#pragma once

#include <twist/support/source_location.hpp>
#include <twist/support/string_builder.hpp>

namespace detail {
void Panic(const std::string& error);
}  // namespace detail

// Print source location and error message to stderr, then abort
// Usage: TWIST_PANIC("Internal error: " << e.what());

#define TWIST_PANIC(error)                                              \
  do {                                                                  \
    ::detail::Panic(twist::StringBuilder() << HERE() << ": " << error); \
  } while (false)
