#pragma once

#include <twist/support/compiler.hpp>
#include <twist/support/panic.hpp>

#define TWIST_VERIFY(cond, error)                                   \
  do {                                                              \
    if (TWIST_UNLIKELY(!(cond))) {                                  \
      TWIST_PANIC("Assertion '" << #cond << "' failed: " << error); \
    }                                                               \
  } while (false);
