#pragma once

#include <cstdlib>

// Suppress compiler warnings about unused variable/argument
#define TWIST_UNUSED(x) \
  do {                  \
    (void)(x);          \
  } while (false)

// Unreachable by control flow
#define TWIST_UNREACHABLE() std::abort()

// https://llvm.org/docs/BranchWeightMetadata.html#builtin-expect
#define TWIST_LIKELY(cond) __builtin_expect(!!(cond), 1)
#define TWIST_UNLIKELY(cond) __builtin_expect(!!(cond), 0)

// https://preshing.com/20120625/memory-ordering-at-compile-time/
#define TWIST_COMPILER_BARRIER() asm volatile("" ::: "memory")
