#pragma once

#if defined(TWIST_FAULTY)

#include <twist/fault/wrappers/futex.hpp>

namespace twist {
namespace twisted {

using Futex = ::twist::fault::FaultyFutex;

}  // namespace twisted
}  // namespace twist

#else

#include <twist/strand/futex.hpp>

namespace twist {
namespace twisted {

using Futex = ::twist::strand::Futex;

}  // namespace twisted
}  // namespace twist

#endif
