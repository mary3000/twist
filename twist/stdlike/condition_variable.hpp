#pragma once

#if defined(TWIST_FAULTY)

#include <twist/fault/wrappers/condvar.hpp>

namespace twist {
namespace stdlike {

using condition_variable = fault::FaultyConditionVariable;  // NOLINT

}  // namespace stdlike
}  // namespace twist

#else

#include <condition_variable>

namespace twist {
namespace stdlike {

using std::condition_variable;

}  // namespace stdlike
}  // namespace twist

#endif
