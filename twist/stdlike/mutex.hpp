#pragma once

#if defined(TWIST_FAULTY)

#include <twist/fault/wrappers/mutex.hpp>

namespace twist {
namespace stdlike {

using mutex = fault::FaultyMutex;  // NOLINT

}  // namespace stdlike
}  // namespace twist

#else

#include <mutex>

namespace twist {
namespace stdlike {

using ::std::mutex;

}  // namespace stdlike
}  // namespace twist

#endif
