#pragma once

#if defined(TWIST_FAULTY)
#include <twist/fault/wrappers/thread.hpp>
#endif

#include <twist/strand/stdlike.hpp>

namespace twist {
namespace stdlike {

#if defined(TWIST_FAULTY)

using thread = twist::fault::FaultyThread;

#else

// https://en.cppreference.com/w/cpp/thread/thread
using thread = strand::thread;  // NOLINT

#endif

namespace this_thread {

// https://en.cppreference.com/w/cpp/thread/yield
inline void yield() {  // NOLINT
  strand::this_thread::yield();
}

// https://en.cppreference.com/w/cpp/thread/sleep_for
inline void sleep_for(std::chrono::nanoseconds delay) {  // NOLINT
  strand::this_thread::sleep_for(delay);
}

}  // namespace this_thread

}  // namespace stdlike
}  // namespace twist
