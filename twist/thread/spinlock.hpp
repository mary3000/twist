#pragma once

namespace twist {
namespace thread {

// Relax in favour of the CPU owning the lock

// https://c9x.me/x86/html/file_module_x86_id_232.html
// https://aloiskraus.wordpress.com/2018/06/16/why-skylakex-cpus-are-sometimes-50-slower-how-intel-has-broken-existing-code/

inline void SpinLockPause() {
  asm volatile("pause\n" : : : "memory");
}

}  // namespace thread
}  // namespace twist
