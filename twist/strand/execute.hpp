#pragma once

#include <functional>

namespace twist {
namespace strand {

void ExecuteInStrandContext(std::function<void()> routine);

}  // namespace strand
}  // namespace twist
