#pragma once

#if defined(TWIST_FIBER)

#include <twist/fiber/emulate/futex.hpp>

namespace twist {
namespace strand {

using Futex = ::twist::fiber::FutexLike;

}  // namespace strand
}  // namespace twist

#else

#include <twist/thread/native_futex.hpp>

namespace twist {
namespace strand {

using Futex = ::twist::thread::Futex;

}  // namespace strand
}  // namespace twist

#endif
