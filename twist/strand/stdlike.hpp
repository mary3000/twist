#pragma once

#include <chrono>

#if defined(TWIST_FIBER)

// cooperative user-space fibers

#include <twist/fiber/core/api.hpp>
#include <twist/fiber/sync/mutex.hpp>
#include <twist/fiber/sync/condvar.hpp>
#include <twist/fiber/emulate/thread.hpp>

namespace twist {
namespace strand {

using thread = fiber::ThreadLike;  // NOLINT

using thread_id = fiber::FiberId;  // NOLINT

extern const thread_id kInvalidThreadId;

namespace this_thread {

inline thread_id get_id() {  // NOLINT
  return fiber::GetFiberId();
}

// scheduling

inline void yield() {  // NOLINT
  fiber::Yield();
}

inline void sleep_for(Duration duration) {  // NOLINT
  fiber::SleepFor(duration);
}

}  // namespace this_thread

using mutex = fiber::Mutex;                           // NOLINT
using condition_variable = fiber::ConditionVariable;  // NOLINT

}  // namespace strand
}  // namespace twist

#else

// native threads

#include <twist/support/time.hpp>

#include <thread>
#include <mutex>
#include <condition_variable>

namespace twist {
namespace strand {

using thread = std::thread;  // NOLINT

using thread_id = ::std::thread::id;  // NOLINT

extern const thread_id kInvalidThreadId;

namespace this_thread {

inline thread_id get_id() {  // NOLINT
  return ::std::this_thread::get_id();
}

// scheduling

inline void yield() {  // NOLINT
  ::std::this_thread::yield();
}

// NOLINTNEXTLINE
inline void sleep_for(Duration duration) {
  std::this_thread::sleep_for(duration);
}

}  // namespace this_thread

using mutex = std::mutex;  // NOLINT

using condition_variable = std::condition_variable;  // NOLINT

}  // namespace strand
}  // namespace twist

#endif
