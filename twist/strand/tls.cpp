#include <twist/strand/tls.hpp>

#include <twist/fiber/core/scheduler.hpp>

namespace twist {
namespace strand {

// NB: destroyed before TLSManager
static thread_local ManagedTLS tls;

TLS& TLSManager::GetTLS() {
  auto* fiber = fiber::GetCurrentFiber();
  if (fiber) {
    return fiber->GetFLS();
  } else {
    return tls.Get();
  }
}

}  // namespace strand
}  // namespace twist
