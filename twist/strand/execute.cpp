#include <twist/strand/execute.hpp>

#if defined(TWIST_FIBER)
#include <twist/fiber/core/api.hpp>
#endif

namespace twist {
namespace strand {

void ExecuteInStrandContext(std::function<void()> routine) {
#if defined(TWIST_FIBER)
  twist::fiber::RunScheduler(routine);
#else  // threads
  routine();
#endif
}

}  // namespace strand
}  // namespace twist
