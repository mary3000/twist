#pragma once

#include <twist/strand/tls.hpp>

namespace twist {
namespace strand {

template <typename T>
class ThreadLocal {
 public:
  ThreadLocal(TLSManager::Ctor ctor, TLSManager::Dtor dtor) {
    Init(ctor, dtor);
  }

  ThreadLocal(T default_value = T{}) {
    auto ctor = [v = std::move(default_value)]() -> void* {
      return new T(std::move(v));
    };
    auto dtor = [](void* ptr) { delete (T*)ptr; };

    Init(ctor, dtor);
  }

  T& Get() {
    return *GetPointer();
  }

  T& operator*() {
    return Get();
  }

  T* operator->() {
    return GetPointer();
  }

 private:
  void Init(TLSManager::Ctor ctor, TLSManager::Dtor dtor) {
    slot_index_ = TLSManager::Instance().AcquireSlot(ctor, dtor);
  }

  T* GetPointer() {
    T* data = ((T*)TLSManager::Instance().Access(slot_index_));
    TWIST_VERIFY(data != nullptr, "Not initialized");
    return data;
  }

 private:
  size_t slot_index_;
};

}  // namespace strand
}  // namespace twist
