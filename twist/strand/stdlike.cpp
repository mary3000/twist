#include <twist/strand/stdlike.hpp>

namespace twist {
namespace strand {

#if defined(TWIST_FIBER)

const thread_id kInvalidThreadId = -1;

#else

const std::thread::id kInvalidThreadId{};

#endif

}  // namespace strand
}  // namespace twist
