#pragma once

#include <twist/thread/spinlock.hpp>
#include <twist/strand/stdlike.hpp>

#include <thread>

namespace twist {
namespace strand {

class SpinWait {
  static const size_t kIterationsBeforeYield = 100;

 public:
  void operator()() noexcept {
    SpinOnce();
  }

  void SpinOnce() noexcept;

  void Reset() {
    iteration_count_ = 0;
  }

 private:
  // Implementation for threads backend
  void SpinOnceMultiCore() {
    iteration_count_++;
    if (iteration_count_ < kIterationsBeforeYield) {
      twist::thread::SpinLockPause();
    } else {
      std::this_thread::yield();
    }
  }

  // Implementation for fibers backend
  void SpinOnceSingleCore() {
    // Yield immediately
    twist::strand::this_thread::yield();
  }

 private:
  size_t iteration_count_{0};
};

}  // namespace strand
}  // namespace twist
