#pragma once

#include <twist/fault/adversary/adversary.hpp>

#include <twist/strand/spin_wait.hpp>
#include <twist/strand/stdlike.hpp>

#include <twist/logging/logging.hpp>

#include <atomic>
#include <chrono>

namespace twist {
namespace ratelimit {

////////////////////////////////////////////////////////////////////////////////

class Semaphore {
 public:
  Semaphore(size_t total) : total_(total), permits_(total) {
  }

  void Acquire() {
    twist::SpinWait spin_wait;
    while (!TryAcquire()) {
      spin_wait();
    }
  }

  bool TryAcquire() {
    while (true) {
      size_t permits = permits_.load();
      if (permits == 0) {
        return false;
      } else if (permits_.compare_exchange_weak(permits, permits - 1)) {
        return true;
      }
    }
  }

  void AcquireAll() {
    size_t collected = 0;

    twist::SpinWait spin_wait;
    while (collected < total_) {
      size_t current = permits_.exchange(0);
      if (current > 0) {
        collected += current;
      } else {
        spin_wait();
      }
    }
  }

  void Release(size_t count) {
    permits_.fetch_add(count);
  }

 private:
  size_t total_;
  std::atomic<size_t> permits_;
};

////////////////////////////////////////////////////////////////////////////////

// Produce quiescent points!

class QuiescentRateLimiter {
 public:
  QuiescentRateLimiter(size_t threads, std::chrono::nanoseconds qpoint_delay)
      : threads_(threads),
        permits_(threads),
        qpoint_delay_(qpoint_delay),
        shaper_([this]() { ShapeTraffic(); }) {
  }

  void Enter() {
    if (!permits_.TryAcquire()) {
      PassSlow();
    }
  }

  void Exit() {
    permits_.Release(1);
  }

  size_t QuiescentPointCount() const {
    return qpoint_count_.load();
  }

  void Stop() {
    stop_requested_.store(true);
    shaper_.join();
  }

 private:
  void PassSlow() {
    twist::fault::GetAdversary()->Exit();
    permits_.Acquire();
    twist::fault::GetAdversary()->Enter();
  }

  void ShapeTraffic() {
    while (!stop_requested_.load()) {
      twist::strand::this_thread::sleep_for(qpoint_delay_);

      permits_.AcquireAll();

      // Quiescent point!
      qpoint_count_.fetch_add(1);

      permits_.Release(threads_);
    }
  }

 private:
  size_t threads_;
  Semaphore permits_;
  std::chrono::nanoseconds qpoint_delay_;
  std::atomic<bool> stop_requested_{false};
  std::atomic<size_t> qpoint_count_{0};

  twist::strand::thread shaper_;
};

}  // namespace ratelimit
}  // namespace twist
